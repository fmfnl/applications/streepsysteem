CREATE DATABASE IF NOT EXISTS `streepsysteem` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `streepsysteem`;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `streepsysteem`
--

-- --------------------------------------------------------

create table debits
(
    debit_id    int unsigned auto_increment
        primary key,
    timestamp   timestamp                        default current_timestamp() not null,
    description varchar(255)                                                 null,
    BTW         enum ('0', '6', '21', 'unknown') default 'unknown'           not null
)
    engine = InnoDB;

create index BTW
    on debits (BTW);

create index timestamp
    on debits (timestamp);

create table exact_users
(
    exact_id int(11) unsigned not null,
    name     varchar(255)     not null,
    constraint exact_id
        unique (exact_id)
)
    engine = InnoDB;

create table nfc
(
    relation_id int auto_increment
        primary key,
    user_id     int       default null,
    hex         varchar(255)                          not null,
    description varchar(255)                          not null,
    last_used   timestamp default current_timestamp() not null on update current_timestamp(),
    constraint hex
        unique (hex)
)
    engine = InnoDB;

create index last_used
    on nfc (last_used);

create index user_id
    on nfc (user_id);

create table products
(
    product_id   int auto_increment
        primary key,
    product_name varchar(255)                           not null,
    price        int                        default 0   not null,
    barcode      varchar(255)                           not null,
    beer         enum ('yes', '')                       not null,
    BTW          enum ('0', '6', '9', '21') default '9' not null,
    constraint barcode
        unique (barcode)
)
    engine = InnoDB;

create index price
    on products (price);

create index product_name
    on products (product_name);

create table productsbackup
(
    product_id   int auto_increment
        primary key,
    product_name varchar(255)                           not null,
    price        int                        default 0   not null,
    barcode      varchar(255)                           not null,
    beer         enum ('yes')                           not null,
    BTW          enum ('0', '6', '9', '21') default '9' not null,
    constraint barcode
        unique (barcode)
)
    engine = InnoDB;

create index price
    on productsbackup (price);

create index product_name
    on productsbackup (product_name);

create table transaction_products
(
    transaction_id     int not null,
    product_id         int not null,
    transaction_price  int not null,
    transaction_amount int not null
)
    engine = InnoDB;

create index product_id
    on transaction_products (product_id);

create index transaction_id
    on transaction_products (transaction_id, product_id);

create index transaction_id_2
    on transaction_products (transaction_id);

create table transactions
(
    transaction_id int auto_increment
        primary key,
    user_id        int                                   not null,
    date           timestamp default current_timestamp() not null,
    debit_id       int unsigned                          not null
)
    engine = InnoDB;

create index date
    on transactions (date);

create index debit_id
    on transactions (debit_id);

create index user_id
    on transactions (user_id);

create table users
(
    user_id int auto_increment
        primary key,
    fmf_id  varchar(255) not null,
    name    varchar(255) not null,
    email   varchar(255) not null
)
    engine = InnoDB;

create index email
    on users (email);

create index fmf_id
    on users (fmf_id);

create index name
    on users (name);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
