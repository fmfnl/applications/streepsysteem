ALTER TABLE
    nfc
MODIFY COLUMN
    user_id
        int unsigned NULL
        DEFAULT null;

UPDATE nfc
SET user_id = null
WHERE user_id = 0;

ALTER TABLE
    transactions
MODIFY COLUMN
    debit_id
        int unsigned NULL
        DEFAULT null;

UPDATE transactions
SET debit_id = null
WHERE debit_id = 0;

