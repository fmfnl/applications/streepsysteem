<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Debit extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	private $debit_id;
	private $btw_percentage;
	
	public function index()
	{
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
		$this->load->database();
		
		$this->grocery_crud->set_subject('User');
		$this->grocery_crud->set_table('users');
		if ($this->grocery_crud->getState() == 'ajax_list' || $this->grocery_crud->getState() == 'list')
			$this->grocery_crud->columns('name', 'fmf_id', 'debt', 'num_transactions');
		if ($this->grocery_crud->getState() == 'export')
			$this->grocery_crud->columns('fmf_id', 'grootboek_nummer', 'btw_code', 'debt', 'omschrijving');
		$this->grocery_crud->unset_add();
		$this->grocery_crud->unset_edit();
		$this->grocery_crud->unset_export();
		$this->grocery_crud->unset_print();
		$this->grocery_crud->unset_read();
		$this->grocery_crud->unset_delete();
		
		$this->grocery_crud->display_as('fmf_id','Koppeling Exact Online');
		$this->grocery_crud->display_as('name','Name');
		$this->grocery_crud->display_as('debt','Debt');
		$this->grocery_crud->display_as('user_id','Num Transactions');		
		$this->grocery_crud->display_as('grootboek_nummer','Grootboeknummer');
		$this->grocery_crud->display_as('btw_code', 'BTW Code');
		$this->grocery_crud->order_by('name','asc');
		
		$this->grocery_crud->callback_column('num_transactions', array($this, 'get_user_num_transactions'));		
		$this->grocery_crud->callback_column('debt', array($this, 'get_user_total_debt'));
		$this->grocery_crud->callback_column('grootboek_nummer', array($this, 'get_grootboek_nummer'));
		$this->grocery_crud->callback_column('btw_code', array($this, 'get_btw_code'));
		$this->grocery_crud->callback_column('omschrijving', array($this, 'get_omschrijving'));
		
		$this->grocery_crud->add_action('user_paid', base_url('assets/ico/check.png'), 'debit/user_paid');

        $this->grocery_crud->where('(SELECT COUNT(`transaction_id`) FROM `transactions` WHERE `debit_id` is null AND `transactions`.`user_id` = `users`.`user_id`) !=', '0');
		
        $output = $this->grocery_crud->render();
		$output->total = $this->format_money($this->get_total_debt());
        $this->view_users($output);	
    }

    public function user_paid($user_id)
    {
    	if (intval($user_id) > 0)
    	{
			$this->load->helper('url');
	    	$this->load->database();
			$query = "INSERT INTO `debits` (`debit_id`, `timestamp`, `description`) VALUES (NULL, NOW(), NULL);";

			$this->db->query($query);

			$debit_id = $this->db->insert_id();

			$query = "UPDATE `transactions` SET `debit_id` = " . $this->db->escape(intval($debit_id)) .  " 
					  WHERE `debit_id` is null AND `transactions`.`user_id` = " . $this->db->escape(intval($user_id));

	 		$q = $this->db->query($query);
		}
    	
 		redirect('debit');
    }
    
    public function user_not_paid($user_id)
    {
    	if (intval($user_id) > 0)
    	{
			$this->load->helper('url');
	    	$this->load->database();
			$query = "INSERT INTO `debits` (`debit_id`, `timestamp`, `description`) VALUES (NULL, NOW(), NULL);";

			$this->db->query($query);

			$debit_id = $this->db->insert_id();

			$query = "UPDATE `transactions` SET `debit_id` = " . $this->db->escape(intval($debit_id)) .  " 
					  WHERE `debit_id` is null AND `transactions`.`user_id` = " . $this->db->escape(intval($user_id));

	 		$q = $this->db->query($query);
		}
    	
 		redirect('debit');
    }
    
	public function history()
	{
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
		$this->load->database();
		
		$this->grocery_crud->set_table('debits');
		$this->grocery_crud->callback_before_delete(array($this, 'clear_transactions'));
		
		$this->grocery_crud->unset_add();
		$this->grocery_crud->unset_read();
		
		$this->grocery_crud->field_type('timestamp', 'readonly');
		$this->grocery_crud->edit_fields('timestamp', 'description');
		if ($this->grocery_crud->getState() != "post")
			$this->grocery_crud->columns('timestamp', 'description', 'sum');
		$this->grocery_crud->callback_column('sum', array($this, 'get_debit_sum'));
		
		$this->grocery_crud->order_by('timestamp','desc');
		$this->grocery_crud->add_action('view_debit', '', 'debit/view_debit', 'read-icon');
		
		$output = $this->grocery_crud->render();
        $this->view_debit_hist($output);	
	}
	
	public function view_debit($debit_id, $btw_percentage = null)
	{	
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
		$this->load->database();
		
		$this->debit_id = $debit_id;
		if (!is_null($btw_percentage))
			$this->btw_percentage = $btw_percentage;
		
		$this->grocery_crud->set_subject('User');
		$this->grocery_crud->set_table('users');
		if ($this->grocery_crud->getState() == 'ajax_list' || $this->grocery_crud->getState() == 'list')
			$this->grocery_crud->columns('name', 'fmf_id', 'debt', 'omschrijving');
		if ($this->grocery_crud->getState() == 'export')
			$this->grocery_crud->columns('fmf_id', 'grootboek_nummer', 'btw_code', 'debt', 'omschrijving', 'name');
		$this->grocery_crud->unset_add();
		$this->grocery_crud->unset_edit();
		$this->grocery_crud->unset_read();
		$this->grocery_crud->unset_delete();
		
		$this->grocery_crud->display_as('fmf_id','Koppeling Exact Online');
		$this->grocery_crud->display_as('name','Name');
		$this->grocery_crud->display_as('debt','Debt');
		$this->grocery_crud->display_as('btw_code', 'BTW Code');
		$this->grocery_crud->display_as('user_id','Num Transactions');		
		$this->grocery_crud->display_as('grootboek_nummer','Grootboeknummer');
		$this->grocery_crud->order_by('name','asc');
		
		$this->grocery_crud->callback_column('debt', array($this, 'get_user_debit_debt'));
		$this->grocery_crud->callback_column('grootboek_nummer', array($this, 'get_grootboek_nummer'));
		$this->grocery_crud->callback_column('btw_code', array($this, 'get_btw_code'));
		$this->grocery_crud->callback_column('omschrijving', array($this, 'get_omschrijving'));
		
		$this->grocery_crud->add_action('user_not_paid', base_url('assets/ico/cross.png'), '', '', array($this, 'debit_not_paid_url_callback'));
		
		$this->grocery_crud->where('(SELECT COUNT(`transaction_id`) FROM `transactions` WHERE `debit_id` = ' . $this->db->escape(intval($debit_id)). ' AND `transactions`.`user_id` = `users`.`user_id`) !=', '0');
		
        $output = $this->grocery_crud->render();
		$output->total = $this->format_money($this->get_total_debt());
		$output->debit_id = $this->debit_id;
		$output->btw_percentage = $this->btw_percentage;
        $this->view_debit_history($output);	
	}
	
    public function debit_not_paid_url_callback($id, $row)
    {
		return site_url('debit/usernotpaid') . '/' . $this->debit_id . '/' . $row->user_id;
	}
	
	public function usernotpaid($debit_id, $user_id)
	{
		$this->load->helper('url');
		$this->load->database();
		$query = "UPDATE `transactions` SET `debit_id` = 0 
				WHERE `transactions`.`debit_id` = " . $this->db->escape(intval($debit_id)) . "
				AND `transactions`.`user_id` = " . $this->db->escape(intval($user_id));
	 		
		$q = $this->db->query($query);
		
		
		redirect('debit/view_debit/' . $debit_id);
	
	}
	
	public function get_debit_sum($key, $row)
	{
		$debit_id = $row->debit_id;
		$query = "SELECT SUM(`transaction_price` * `transaction_amount`) AS `total` FROM `transaction_products` 
				INNER JOIN `transactions` ON `transactions`.`transaction_id` = `transaction_products`.`transaction_id`
				WHERE `debit_id` = '" . $this->db->escape(intval($debit_id)) . "' 
				GROUP BY `debit_id`";
 		
 		$q = $this->db->query($query);
 		
		if ($q->num_rows() == 1)
 			return $this->format_money($q->row()->total);
		else
			return $this->format_money(0.0);
	}
	
	function clear_transactions($debit_id)
	{
		$query = "UPDATE `transactions` SET `debit_id` = 0 WHERE `transactions`.`debit_id` = " . $this->db->escape(intval($debit_id));
	 		
		$q = $this->db->query($query);
		return true;
	}
	
	function get_grootboek_nummer($value, $user)
 	{
		return 1060;
	}
	
	function get_btw_code($value, $user)
 	{
		if (!is_null($this->btw_percentage))
		{
			switch ($this->btw_percentage)
			{
				case '21':
					return 1;
				break;
					
				case '6':
					return 2;
				break;
				
				case '9':
					return 5;
				break;
				
				default:
				case '0':
					return 0;
				break;
			}
		}
		return 0;
	}	
	
	function get_omschrijving($value, $user)
 	{
 		$query = "SELECT `description` FROM `debits` WHERE `debit_id` = " . $this->db->escape(intval($this->debit_id)) . " LIMIT 1;";
 		
 		$q = $this->db->query($query);
 		$description = "";
		if ($q->num_rows() == 1)
 			$description = $q->row()->description;
 		
 		if ($description == "")
 			$description = "Incasso Streepsysteem FMF";
 			
 		if (intval($user->fmf_id > 0))
			$description .= ", lidnummer " . $user->fmf_id;
			
		return $description;
	}
	
	function get_total_debt()
	{	
		$query = "SELECT SUM(`transaction_price` * `transaction_amount`) AS `total` FROM `transaction_products`
				INNER JOIN `transactions` ON `transactions`.`transaction_id` = `transaction_products`.`transaction_id` 
				INNER JOIN `products` ON `products`.`product_id` = `transaction_products`.`product_id`
				WHERE `debit_id` is null";
 		
 		$q = $this->db->query($query);
 		
		if ($q->num_rows() == 1)
 			return $q->row()->total;
		else
			return 0;
	}
	
    function get_user_num_transactions($value, $user)
 	{
		$query = "SELECT COUNT(DISTINCT(`transaction_products`.`transaction_id`)) AS num_transactions 
					FROM `transaction_products` 
					INNER JOIN `transactions` ON `transaction_products`.`transaction_id` = `transactions`.`transaction_id` 
					WHERE `transactions`.`user_id` = " . $this->db->escape(intval($user->user_id)) . "
					AND `transactions`.`debit_id` is null";
 		
 		$q = $this->db->query($query);
 		
		if ($q->num_rows() == 1)
 			return $q->row()->num_transactions;
		else
			return 0;
	}
	
	function directdebit()
	{
		$this->load->database();
		$query = "INSERT INTO `debits` (`debit_id`, `timestamp`, `description`) VALUES (NULL, NOW(), NULL);";

		$this->db->query($query);

		$debit_id = $this->db->insert_id();

		$query = "UPDATE `transactions` SET `debit_id` = " . $this->db->escape(intval($debit_id)) .  " WHERE `transactions`.`debit_id` is null";

 		$q = $this->db->query($query);
 		
 		redirect('debit');
		
	}
	
	function get_user_debit_debt($id, $row)
	{
		return sprintf("<font color='red'>&euro;%.2f</font>", $this->get_user_specific_debit_debt($this->debit_id, $row->user_id) / 100.0);
	}
	
    function get_user_total_debt($value, $user)
 	{
 		return sprintf("<font color='red'>&euro;%.2f</font>", $this->get_user_debt($user->user_id) / 100.0);
	}
	
	function format_money($amount)
	{
		return sprintf("&euro;%.2f", $amount / 100.0);
	}
	
	private function get_user_specific_debit_debt($debit_id, $user_id)
	{
		$btw_percentage = 6;
		if (!is_null($this->btw_percentage))
			$btw_percentage = $this->btw_percentage;
		
		$query = "SELECT `user_id`, SUM(`transaction_price` * `transaction_amount`) AS `debt` 
					FROM `transaction_products` 
					INNER JOIN `transactions` ON `transaction_products`.`transaction_id` = `transactions`.`transaction_id` 
					INNER JOIN `products` ON `products`.`product_id` = `transaction_products`.`product_id` 
					WHERE `transactions`.`user_id` = " . $this->db->escape(intval($user_id)) . "
					AND `transactions`.`debit_id` =  " . $this->db->escape(intval($debit_id)) . "		
					AND `products`.`BTW` = " . $this->db->escape($btw_percentage) . "
					GROUP BY `transactions`.`user_id`";
 		
 		$q = $this->db->query($query);
 		
		if ($q->num_rows() == 1)
 			return $q->row()->debt;
		else
			return 0.0;
	}
	
	private function get_user_debt($user_id)
	{	
		
			
		$query = "SELECT `user_id`, SUM(`transaction_price` * `transaction_amount`) AS `debt` 
					FROM `transaction_products` 
					INNER JOIN `transactions` ON `transaction_products`.`transaction_id` = `transactions`.`transaction_id` 
					INNER JOIN `products` ON `products`.`product_id` = `transaction_products`.`product_id` 
					WHERE `transactions`.`user_id` = " . $this->db->escape(intval($user_id)) . "
					AND `transactions`.`debit_id` is null 		
					GROUP BY `transactions`.`user_id`";
		
 		$q = $this->db->query($query);
 		
		if ($q->num_rows() == 1)
 			return $q->row()->debt;
		else
			return 0.0;
	}
	function view_debit_history($output = null)
    {	
		$this->load->view('template/header', $output);
		$this->load->view('template/menu', $output);     	
        $this->load->view('debit_history', $output);  
        $this->load->view('template/footer', $output);
    }
    function view_debit_hist($output = null)
    {	
		$this->load->view('template/header', $output);
		$this->load->view('template/menu', $output);     	
        $this->load->view('debit_hist', $output);  
        $this->load->view('template/footer', $output);
    }
	
    function view_users($output = null)
    {	
		$this->load->view('template/header', $output);
		$this->load->view('template/menu', $output);     	
        $this->load->view('debit', $output);  
        $this->load->view('template/footer', $output);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
