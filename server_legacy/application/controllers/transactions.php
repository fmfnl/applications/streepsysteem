<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transactions extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{		
		$this->load->library('grocery_CRUD');
		$this->grocery_crud->where('`transactions`.`debit_id`', null);
		$this->grocery_crud->columns('user_id', 'total', 'products', 'date');
		$this->get_transactions();
	}
	
	public function history()
	{			
		$this->load->library('grocery_CRUD');
		$this->grocery_crud->where('`transactions`.`debit_id` is not null');
		$this->grocery_crud->columns('user_id', 'total', 'products', 'date', 'debit_id');
		$this->get_transactions();
	}
	
	public function get_transactions()
	{
		$this->load->helper('url');		
		$this->load->database();
		
		$this->grocery_crud->required_fields('');
		$this->grocery_crud->set_subject('Transaction');
		$this->grocery_crud->set_table('transactions');
		
		
		$this->grocery_crud->display_as('user_id','Name');
		$this->grocery_crud->display_as('date','Date of transaction');
		$this->grocery_crud->display_as('debit_id','Date paid');		

		$this->grocery_crud->unset_edit();
		$this->grocery_crud->unset_add();
	
		$this->grocery_crud->set_relation('user_id','users','name');
		$this->grocery_crud->set_relation('debit_id', 'debits', 'timestamp');
		
		$this->grocery_crud->order_by('date','desc');
		$this->grocery_crud->set_relation_n_n('products', 'transaction_products', 'products', 'transaction_id', 'product_id', '{transaction_amount}x {product_name} (&euro;{transaction_price/100})',null, null, TRUE);
		
		
		$this->grocery_crud->fields('user_id', 'total', 'products', 'date', 'debit_id');
		
		if ($this->uri->segment(2) == 'history')
			$this->grocery_crud->add_action('Not Paid', base_url('assets/ico/cross.png'), 'transactions/unpaid', 'paid');
		else
			$this->grocery_crud->add_action('Paid', base_url('assets/ico/check.png'), 'transactions/paid', 'paid');
		
		$this->grocery_crud->callback_edit_field('total',array($this,'get_transaction_total_field'));
		$this->grocery_crud->callback_column('total', array($this, 'get_transaction_total_column'));
		
        $output = $this->grocery_crud->render();

        $this->view_users($output);	
    }
 	
 	function paid($transaction_id)
 	{
 		$this->load->helper('url');	
 		$this->load->database();
 		if (intval($transaction_id) > 0)
 		{	
 			$query = "INSERT INTO `debits` (`debit_id`, `timestamp`, `description`) VALUES (NULL, NOW(), NULL);";
 			
 			$this->db->query($query);
 			
 			$debit_id = $this->db->insert_id();
 			
			$query = "UPDATE `transactions` SET `debit_id` = " . $this->db->escape(intval($debit_id)) .  " WHERE `transactions`.`transaction_id` = " . $this->db->escape(intval($transaction_id));
	 		
	 		$q = $this->db->query($query);
		}
		redirect('transactions');
 	}
 	
 	function unpaid($transaction_id)
 	{
 		$this->load->helper('url');	
 		$this->load->database();
 		if ($transaction_id > 0)
 		{
 			$query = "SELECT `debit_id` FROM `transactions` WHERE `transactions`.`transaction_id` = " . $this->db->escape(intval($transaction_id));
 			$q = $this->db->query($query);
 			$debit_id = $q->row()->debit_id;
 			
 			if (intval($debit_id) > 0)
 			{
				$query = "UPDATE `transactions` SET `debit_id` = NULL WHERE `transactions`.`transaction_id` = " . $this->db->escape(intval($transaction_id)) . " LIMIT 1;";
	 		
		 		$q = $this->db->query($query);
		 		
 				$query = "SELECT COUNT(`transaction_id`) AS numtransactions FROM `transactions` WHERE `transactions`.`debit_id` = " . $this->db->escape(intval($debit_id));
	 			$q = $this->db->query($query);
	 			$num_transactions = $q->row()->numtransactions;
	 			
	 			if ($num_transactions == 0)
	 			{
			 		$query = "DELETE FROM `debits` WHERE `debit_id` = " . $this->db->escape(intval($debit_id)) . " LIMIT 1;";
			 		
			 		$q = $this->db->query($query);
			 	}
			}
	
		}
		redirect('transactions/history');
 	}
 		
 	function get_transaction_total_column($value, $transaction)
 	{
 		return sprintf("&euro;%.2f", $this->get_transaction_sum($transaction->transaction_id) / 100.0); 		
	}
	
	function get_transaction_total_field($value = '', $primary_key = null)
	{
		return sprintf("&euro;%.2f", $this->get_transaction_sum($primary_key) / 100.0);
	}
	
	private function get_transaction_sum($transaction_id)
	{
		$query = "SELECT SUM(`transaction_price` * `transaction_amount`) AS `total` FROM `transaction_products` WHERE `transaction_id` = '" . $this->db->escape(intval($transaction_id)) . "' GROUP BY `transaction_id`";
 		
 		$q = $this->db->query($query);
 		
		if ($q->num_rows() == 1)
 			return $q->row()->total;
		else
			return 0.0;
	}
	
    function view_users($output = null)
    {
		$this->load->view('template/header', $output);
		$this->load->view('template/menu', $output);
        $this->load->view('transactions', $output);
        $this->load->view('template/footer', $output);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */