<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
		$this->load->database();
		
		$this->grocery_crud->set_subject('User');
		$this->grocery_crud->set_table('users');
		$this->grocery_crud->columns('name', 'fmf_id', 'email');
		
		$this->grocery_crud->display_as('fmf_id','Koppeling Exact Online');
		$this->grocery_crud->display_as('name','Name');
		$this->grocery_crud->display_as('email','Email Address');
		$this->grocery_crud->required_fields('name');
		$this->grocery_crud->order_by('name','asc');
		
		$this->grocery_crud->set_relation('fmf_id', 'exact_users', 'name');
		
        $output = $this->grocery_crud->render();
	
        $this->view_users($output);	
    }
	
	function update()
	{
		error_reporting(E_ALL);
		ini_set("display_errors", 1);
		$this->load->helper('url');
		$this->load->database();
		
		$resource = ldap_connect(getenv("LDAP_HOST"));
		ldap_set_option($resource, LDAP_OPT_PROTOCOL_VERSION, 3);
		ldap_bind($resource, getenv("LDAP_USER"), getenv("LDAP_PASS"));
				
		$results = ldap_list($resource, 'ou=Members,dc=fmf,dc=nl', '(&(objectClass=x-FMFPerson)(x-FMF-exactOnlineCode=*))', array('cn', 'x-FMF-exactOnlineCode'));
		//print($results)
		$entries = ldap_get_entries($resource, $results);
				
		$names = "";
		$count = 0;
		for ($i = 0; $i < $entries['count']; ++$i)
		{
			$person = $entries[$i];
			
			$exact_code = intval($person['x-fmf-exactonlinecode'][0]);
			$name = $this->db->escape($person['cn'][0]);
			
			//check if user exists
			$query = "SELECT COUNT(*) AS `c` FROM `exact_users` WHERE `exact_id`=" . $exact_code;
			$q = $this->db->query($query);
			
			if (intval($q->row()->c) > 0)
				continue;
		
			$query = "INSERT INTO `exact_users` (`exact_id`, `name`) VALUES (" . $exact_code . ", " . $name . ")";
			$names .= $query . " <br/>";
			$q = $this->db->query($query);
			$count++;
		}
		
		$this->load->view('template/header');
		$this->load->view('template/menu');
        $this->load->view('users_update', array('names' => $names, 'count' => $count));  
        $this->load->view('template/footer');
	}
	
    function view_users($output = null)
    {
		$this->load->view('template/header', $output);
		$this->load->view('template/menu', $output);     	
        $this->load->view('users_real', $output);  
        $this->load->view('template/footer', $output);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
