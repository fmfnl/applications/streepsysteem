<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cards extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->library('grocery_CRUD');
        $this->grocery_crud->where('`nfc`.user_id is not null');
		$this->print_crud();
    }
    
	public function unassigned()
	{
		$this->load->library('grocery_CRUD');
		$this->grocery_crud->where('`nfc`.user_id', null);
		$this->grocery_crud->unset_add();
		$this->print_crud();
    }
        
    private function print_crud()
    {
		$this->load->helper('url');
		$this->load->database();
		
		$this->grocery_crud->set_subject('Card');
		$this->grocery_crud->set_table('nfc');
		
		$this->grocery_crud->columns('user_id','hex', 'last_used', 'description');
		$this->grocery_crud->edit_fields('user_id','hex', 'last_used','description');
		
		$this->grocery_crud->display_as('user_id','Name');
		$this->grocery_crud->display_as('hex','Card Serial UID');
		$this->grocery_crud->display_as('last_used','Last Used');
		
		if ($this->grocery_crud->getState() == 'edit')
			$this->grocery_crud->change_field_type('hex', 'readonly');
		
		$this->grocery_crud->order_by('last_used','desc');
		
		$this->grocery_crud->change_field_type('last_used', 'readonly');
		$this->grocery_crud->add_fields('user_id', 'hex');
		
		$this->grocery_crud->set_relation('user_id', 'users', 'name');
		
        $output = $this->grocery_crud->render();

        $this->view_users($output);	
	}
    
    function view_users($output = null)
    {
		$this->load->view('template/header', $output);
		$this->load->view('template/menu', $output);
        $this->load->view('users', $output);
        $this->load->view('template/footer', $output);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */