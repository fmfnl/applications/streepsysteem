<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Charts extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{		
		$this->load->helper('url');
		$this->view_overview();
    }
    function money()
    {
    	$this->load->helper('url');
		$this->load->database();
		//Load in the controller
		$this->load->library('gcharts');
		$this->gcharts->load('LineChart');
		$dataTable = $this->gcharts->DataTable('Geld');		
		$dataTable->addColumn('string', 'date', 'date');
		$dataTable->addColumn('number', 'Geld', 'money');
		
		$config = array(
    		'title' => 'Geld'
		);
		
		for($i = 30; $i >= 0; $i--) 
		{
		    $date = date("Y-m-d", strtotime('-'. $i .' days'));
		    $money = $this->getmoney($date);
			
		    $data[0] = $date;

			if ($money[0]->money != null)
				$data[1] = $money[0]->money / 100.0;
			else
				$data[1] = 0;
			$dataTable->addRow($data);
		}
		
		$this->gcharts->LineChart('Geld')->setConfig($config);
		$this->view_chart('money');
	}
	
    function bier()
    {
    	$this->load->helper('url');
		$this->load->database();
		//Load in the controller
		$this->load->library('gcharts');
		$this->gcharts->load('DonutChart');
		
		$pils = $this->aantalpils();

		$nietpils = $this->aantalnietpils();
		$totaal = $pils + $nietpils;
		
		$this->gcharts->DataTable('Bier')
			->addColumn('string', 'Soorten', 'soort')
			->addColumn('string', 'Amount', 'amount')
			->addRow(array('Bier', $pils))
			->addRow(array('Niet-bier',$nietpils));
		
		$config = array(
			'title' => 'Bier',
			'pieHole' => .4
		);

		$this->gcharts->DonutChart('Bier')->setConfig($config);
		$this->view_chart('bier-donut');
	}
	
    function biergeld()
    {
    	$this->load->helper('url');
		$this->load->database();
		//Load in the controller
		$this->load->library('gcharts');
		$this->gcharts->load('DonutChart');
		
		$pils = $this->aantalpilsgeld() / 100.0;

		$nietpils = $this->aantalnietpilsgeld() / 100.0;
		$totaal = $pils + $nietpils;
		
		$this->gcharts->DataTable('Bier')
			->addColumn('string', 'Soorten', 'soort')
			->addColumn('string', 'Totaalprijs', 'totaalprijs')
			->addRow(array('Bier', $pils))
			->addRow(array('Niet-bier',$nietpils));
		
		$config = array(
			'title' => 'Bier',
			'pieHole' => .4
		);

		$this->gcharts->DonutChart('Bier')->setConfig($config);
		$this->view_chart('bier-donut');
	}
	
	function getmoney($dag)
	{
		$query = "SELECT SUM(`transaction_amount` * `transaction_price`) AS `money` FROM `transaction_products` INNER JOIN `transactions` ON `transactions`.`transaction_id` = `transaction_products`.`transaction_id` INNER JOIN `products` on `transaction_products`.`product_id` = `products`.`product_id` WHERE DATE(`transactions`.`date`) = " . $this->db->escape($dag);
 		
 		$q = $this->db->query($query);
 		
 		return $q->result();
 	}
	
	
    function biersoorten()
    {
    	$this->load->helper('url');
		$this->load->database();
		//Load in the controller
		$this->load->library('gcharts');
		$this->gcharts->load('DonutChart');
		
		$pilsSoorten = $this->getbiersoorten();
		
		$this->gcharts->DataTable('Bier')
			->addColumn('string', 'Soorten', 'soort')
			->addColumn('string', 'Amount', 'amount');

		foreach ($pilsSoorten as $pilsSoort)
		{
			$this->gcharts->DataTable('Bier')->addRow(array($pilsSoort->product_name, intval($pilsSoort->pils)));
		}			
		
		$config = array(
			'title' => 'Bier',
			'pieHole' => .4
		);

		$this->gcharts->DonutChart('Bier')->setConfig($config);
		$this->view_chart('bier-donut');
	}
	
	function getbiersoorten()
	{
		$query = "SELECT product_name, SUM(`transaction_amount`) AS `pils` FROM `transaction_products` INNER JOIN `transactions` ON `transactions`.`transaction_id` = `transaction_products`.`transaction_id` INNER JOIN `products` on `transaction_products`.`product_id` = `products`.`product_id` WHERE `beer` = 'yes' GROUP BY `product_name`";
 		
 		$q = $this->db->query($query);
 		
 		return $q->result();
 	}
	
		
	private function aantalpils()
	{
		$query = "SELECT SUM(`transaction_amount`) AS `pils` FROM `transaction_products` INNER JOIN `transactions` ON `transactions`.`transaction_id` = `transaction_products`.`transaction_id` INNER JOIN `products` on `transaction_products`.`product_id` = `products`.`product_id` WHERE `beer` = 'yes'";
 		
 		$q = $this->db->query($query);
 		
		if ($q->num_rows() == 1)
 			return intval($q->row()->pils);
		else
			return intval("0");
	}  
	
	private function aantalpilsgeld()
	{
		$query = "SELECT SUM(`transaction_amount` * `transaction_price`) AS `pils` FROM `transaction_products` INNER JOIN `transactions` ON `transactions`.`transaction_id` = `transaction_products`.`transaction_id` INNER JOIN `products` on `transaction_products`.`product_id` = `products`.`product_id` WHERE `beer` = 'yes'";
 		
 		$q = $this->db->query($query);
 		
		if ($q->num_rows() == 1)
 			return intval($q->row()->pils);
		else
			return intval("0");
	}  
	
	private function aantalnietpils()
	{
		$query = "SELECT SUM(`transaction_amount`) AS `pils` FROM `transaction_products` INNER JOIN `transactions` ON `transactions`.`transaction_id` = `transaction_products`.`transaction_id` INNER JOIN `products` on `transaction_products`.`product_id` = `products`.`product_id` WHERE `beer` != 'yes'";
 		
 		$q = $this->db->query($query);
 		
		if ($q->num_rows() == 1)
 			return intval($q->row()->pils);
		else
			return intval("0");
	}   
	
	private function aantalnietpilsgeld()
	{
		$query = "SELECT SUM(`transaction_amount` * `transaction_price`) AS `pils` FROM `transaction_products` INNER JOIN `transactions` ON `transactions`.`transaction_id` = `transaction_products`.`transaction_id` INNER JOIN `products` on `transaction_products`.`product_id` = `products`.`product_id` WHERE `beer` != 'yes'";
 		
 		$q = $this->db->query($query);
 		
		if ($q->num_rows() == 1)
 			return intval($q->row()->pils);
		else
			return intval("0");
	}   
	
    function view_chart($graph)
    {
    	
		$this->load->view('template/header');
		$this->load->view('template/menu');
		switch($graph)
		{
			case 'bier-donut':
				$this->load->view('charts/bier-donut');
			break;
			case 'money':
				$this->load->view('charts/money');
			break;			
		}
        
        $this->load->view('template/footer');
    }
	
    function view_overview($output = null)
    {
		$this->load->view('template/header', $output);
		$this->load->view('template/menu', $output);     	
        $this->load->view('charts-overview', $output);  
        $this->load->view('template/footer', $output);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
