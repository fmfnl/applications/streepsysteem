<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
		$this->load->database();
		
		
		$this->grocery_crud->set_subject('Product');
		$this->grocery_crud->set_table('products');
		$this->grocery_crud->columns('product_name', 'price', 'amount_bought', 'barcode', 'beer', 'BTW');
		$this->grocery_crud->fields('product_name', 'price', 'amount_bought', 'barcode', 'beer', 'BTW');
		$this->grocery_crud->display_as('user_id','User ID');
		$this->grocery_crud->display_as('product_name','Product Name');
		$this->grocery_crud->display_as('amount_bought','Amount Bought');
		$this->grocery_crud->display_as('BTW','BTW (&#37;)');
		
		$this->grocery_crud->order_by('product_name','asc');
		
		if ($this->grocery_crud->getState() == 'ajax_list' || $this->grocery_crud->getState() == 'list')
			$this->grocery_crud->display_as('price','Price');			
		else
			$this->grocery_crud->display_as('price','Price (in cents)');
			
		$this->grocery_crud->required_fields('product_name', 'price', 'barcode');
		$this->grocery_crud->callback_column('price', function($value, $row) {
			return sprintf("&euro;%.2f", $value / 100.0);
		});
	
		
		
		$this->grocery_crud->callback_column('amount_bought', array($this, 'get_product_total_column'));
		$this->grocery_crud->callback_field('amount_bought', array($this, 'get_product_total_column'));
        $output = $this->grocery_crud->render();

        $this->view_users($output);	
    }
	
	
	
	
	
	function get_product_total_column($value, $product)
	{
		if (is_object($product))
			$product = $product->product_id;
		$query = "SELECT SUM(`transaction_amount`) AS `total` FROM `transaction_products` INNER JOIN `transactions` on `transactions`.`transaction_id` = `transaction_products`.`transaction_id` WHERE `product_id` = '" . $this->db->escape(intval($product)) . "'AND date > '2015-08-27 9:12:00' GROUP BY `product_id`";
 		
 		$q = $this->db->query($query);
 		
		if ($q->num_rows() == 1)
 			return $q->row()->total;
		else
			return "0";
	}
	
    function view_users($output = null)
    {
		$this->load->view('template/header', $output);
		$this->load->view('template/menu', $output);
		
			
        $this->load->view('products', $output);  
        $this->load->view('template/footer', $output);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */