<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#">FMF Streepsysteem</a>
	</div>
	<div class="collapse navbar-collapse">
		<ul class="nav navbar-nav">			
			<!--<li<?php echo ($this->uri->segment(1)=='') ? " class='active'" : ""?>><a href="<?php echo site_url();?>">Home</a></li>-->
			<li<?php echo ($this->uri->segment(1)=='transactions') && ($this->uri->segment(2)!='history') ? " class='active'" : ""?>><a href="<?php echo site_url('transactions');?>">Recent Transactions</a></li>
			<!--<li<?php echo ($this->uri->segment(1)=='transactions') && ($this->uri->segment(2)=='history') ? " class='active'" : ""?>><a href="<?php echo site_url('transactions/history');?>">Transaction History</a></li>
			--><li<?php echo ($this->uri->segment(1)=='debit') && ($this->uri->segment(2)=='') ? " class='active'" : ""?>><a href="<?php echo site_url('debit');?>">Open Debt</a></li>
			<li<?php echo ($this->uri->segment(1)=='debit') && ($this->uri->segment(2)=='history' || $this->uri->segment(2)=='view_debit') ? " class='active'" : ""?>><a href="<?php echo site_url('debit/history');?>">Debit History</a></li>
			<li<?php echo ($this->uri->segment(1)=='products') ? " class='active'" : ""?>><a href="<?php echo site_url('products');?>">Products</a></li>
			<li<?php echo ($this->uri->segment(1)=='users') ? " class='active'" : ""?>><a href="<?php echo site_url('users');?>">Users</a></li>
			<li<?php echo ($this->uri->segment(1)=='cards') && ($this->uri->segment(2)!='unassigned') ? " class='active'" : ""?>><a href="<?php echo site_url('cards');?>">Cards</a></li>
			<li<?php echo ($this->uri->segment(1)=='cards') && ($this->uri->segment(2)=='unassigned')? " class='active'" : ""?>><a href="<?php echo site_url('cards/unassigned');?>">Unassigned Cards</a></li>
			<li<?php echo ($this->uri->segment(1)=='charts') ? " class='active'" : ""?>><a href="<?php echo site_url('charts');?>">Charts</a></li>
		</ul>
	</div><!--/.nav-collapse -->
	</div>
</div>