<div class="container">

  <div class="message">
    <h1>404 Page not found</h1>
    <p class="lead">The page you requested was not found.</p>
  </div>

</div><!-- /.container -->

