<?php foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
<?php endforeach; ?>

<?php if (!is_null($debit_id)): ?>
<div class="container">
	<div class="message">
		<button class="btn btn-success btn-medium"><a style="color: white;" href="/index.php/debit/view_debit/<?=$debit_id?>/9">9% btw</a></button>
		<button class="btn btn-success btn-medium"><a style="color: white;" href="/index.php/debit/view_debit/<?=$debit_id?>/21">21% btw</a></button>
	</div>
</div><!-- /.container -->
<?php endif; ?>

<?php if (!is_null($btw_percentage)): ?>
<div class="container">
	<div class="message">
		<?php echo $output; ?>
	</div>
</div><!-- /.container -->

<?php foreach($js_files as $file): ?>

<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<?php endif; ?>