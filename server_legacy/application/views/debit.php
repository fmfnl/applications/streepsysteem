<?php foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
<?php endforeach; ?>"
"
<div class="container">
	<div class="message">
		<button class="btn btn-danger btn-large direct-debit">DIRECT DEBIT (<?php echo $total?>)</button>
	</div>
</div><!-- /.container -->


<div class="container">
	<div class="message">
		<?php echo $output; ?>
	</div>
</div><!-- /.container -->

<?php foreach($js_files as $file): ?>

<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

<script type="text/javascript">
	$(function() {
		$(".direct-debit").click(function() {
			if (confirm("Are you sure? All current open transactions will be marked as paid!"))
			{
				$.get('<?php echo site_url('debit/directdebit')?>', {async: false});
				$("#ajax_refresh_and_loading").click();
			}
		});
	});
</script>