<div class="container">
	<div class="message">
		<button class="btn btn-success btn-large"><a style='color: inherit;' href='/index.php/users/update'>UPDATE</a></button>
	</div>
</div><!-- /.container -->

<?php foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
<?php endforeach; ?>
<div class="container">
	<div class="message">
		<?php echo $output; ?>
	</div>
</div><!-- /.container -->

<?php foreach($js_files as $file): ?>

<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>