import * as esbuild from "esbuild";
import { htmlPlugin } from "@craftamap/esbuild-plugin-html";

const serve = process.env.ESBUILD_SERVE === "1";
const api_url = process.env.API_URL || "";

const entryPoints = ["src/main.tsx"];

const ctx = await esbuild.context({
  entryPoints,
  minify: !serve,
  bundle: true,
  metafile: true,
  sourcemap: true,
  outdir: "dist",
  format: "esm",
  target: ["es2020"],
  assetNames: "assets/[name]",
  loader: { ".png": "file" },
  define: {
    "process.env.API_URL": JSON.stringify(api_url),
  },
  plugins: [
    htmlPlugin({
      files: [
        {
          entryPoints,
          filename: "index.html",
          htmlTemplate: "src/index.html",
          scriptLoading: "module",
        },
      ],
    }),
  ],
});

if (serve) {
  const { port, host } = await ctx.serve({ servedir: "dist" });
  console.log(`Serving on ${host}:${port}`);
} else {
  await ctx.rebuild();
  await ctx.dispose();
  console.log(`Project bundled successfully`);
}
