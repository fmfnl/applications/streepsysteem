import { h, Fragment } from "preact"
import { State, getProductByBarcode, initialState } from "./state"
import { useObservable } from "src/utils/streams"
import type { Observable } from "rxjs"

type Props = {
	states: Observable<State>
}

export function App(props: Props) {
	const state = useObservable(props.states, initialState)

	// *Cries in no pattern matching*
	let content = <></>

	if (state.ui.type === "idle") {
		content = <>Place your card on the device to begin!</>
	} else if (state.ui.type === "connectingCard") {
		content = (
			<p>
				It looks like your card does not exist in our database. But fear not —
				you can contact a board member to get this sorted!
			</p>
		)
	} else if (state.ui.type === "cardConnectionFailed") {
		content = (
			<>
				<p>
					It looks like your card does not exist in our database. Sadly, we
					cannot fix this right now.
				</p>
				<p>
					This most likely means the machine is not connected to the internet.
				</p>
			</>
		)
	} else if (state.ui.type === "shopping") {
		const cart = [...state.ui.products].flatMap(([barcode, quantity]) => {
			const product = getProductByBarcode(barcode, state.products)

			return product === undefined ? [] : [{ ...product, quantity }]
		})

		const totalPrice = cart.reduce(
			(total, product) => total + product.price * product.quantity,
			0
		)

		content = (
			<>
				<div id="shopping-header">
					Scan products to add them to your cart. Pick up your card to proceed
					with payment.
				</div>
				<hr id="shopping-separator" />
				<div id="shopping-products">
					{cart.map((cartItem) => {
						return (
							<>
								<div class="shopping-item-quantity">{cartItem.quantity}</div>
								<div class="shopping-item-cross-icon">⨯</div>{" "}
								<div class="shopping-item-label">{cartItem.name}</div>
								<div class="shopping-item-value">
									{cartItem.price.toFixed(2)}€
								</div>
							</>
						)
					})}
				</div>
				<div id="shopping-total">
					<div id="shopping-total-label">Total:</div>
					<div id="shopping-total-value">{totalPrice.toFixed(2)}€</div>
				</div>
			</>
		)
	}

	return (
		<div id="content" class={state.ui.type}>
			{content}
		</div>
	)
}
