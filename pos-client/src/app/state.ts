import type {
	CardHex,
	Product,
	ProductBarcode,
	Products,
	User,
	UserId,
	Users
} from "../external/api"

export type UserState =
	| { type: "idle" }
	| { type: "connectingCard"; hex: CardHex }
	| { type: "cardConnectionFailed" }
	| {
			type: "shopping"
			hex: CardHex
			user: UserId
			products: Map<ProductBarcode, number>
	  }

export interface State {
	ui: UserState
	users: Users
	products: Products
}

export const idle: UserState = { type: "idle" }
export const cardConnectionFailed: UserState = { type: "cardConnectionFailed" }
export const connectingCard = (hex: CardHex): UserState => ({
	type: "connectingCard",
	hex
})
export const shopping = (user: UserId, hex: CardHex): UserState => ({
	type: "shopping",
	user,
	hex,
	products: new Map()
})

export const initialState: State = {
	users: [],
	products: [],
	ui: idle
}

export function getProductByBarcode(
	barcode: string,
	products: Products
): Product | undefined {
	return products.find((p) => p.barcode === barcode)
}

export function getUserByCardHex(hex: CardHex, users: Users): User | undefined {
	return users.find((user) => user.cards.some((card) => card.hex === hex))
}
