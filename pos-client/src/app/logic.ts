import { barcodeEvents } from "../external/barcode"
import { sleep } from "../utils/helpers"
import { nfcEvents } from "../external/nfc"
import * as S from "./state"
import * as Api from "../external/api"
import * as R from "rxjs"

// The main entrypoint for the logic of the client.
//
// Basic idea:
// - the in-memory mutable state lives in a mutable local variable
// - we subscribe to both barcode and nfc events and react accordingly
// - every 10s, we try to contact the server
//
// After each such update, we re-emit the state, which can then be used by the
// rendering code.
export function state(pollingInterval = 10000): R.Observable<S.State> {
	return new R.Observable((subscriber) => {
		let state: S.State = {
			products: [],
			users: [],
			ui: S.idle
		}

		const barcodeEventStream = barcodeEvents()
		const nfcEventStream = nfcEvents()

		// Subscribe to nfc events
		nfcEventStream.subscribe(async (event) => {
			if (event.type === "connected") {
				const user = S.getUserByCardHex(event.hex, state.users)

				// no user found for this card — time to tell the server!
				if (user === undefined) {
					const pushed = await Api.pushCard(event.hex)
					if (pushed) state.ui = S.connectingCard(event.hex)
					// we are probably offline, so we tell the user we cannot bind
					// their card right now.
					else state.ui = S.cardConnectionFailed
				} else {
					// Otherwise, the card is owned by some user, so we are ready to
					// start shopping!
					state.ui = S.shopping(user.id, event.hex)
				}
			} else if (event.type === "released") {
				// When the card is released, we finalize the current transactions, and
				// return back to the home screen.
				if (state.ui.type === "shopping") {
					Api.appendTransactionWal({
						user: state.ui.user,
						time: Date.now(),
						products: [...state.ui.products]
							.filter(([_, amount]) => amount !== 0)
							.flatMap(([barcode, amount]) => {
								const product = S.getProductByBarcode(barcode, state.products)

								return product === undefined ? [] : [{ ...product, amount }]
							})
					})
				}

				state.ui = S.idle
			}

			subscriber.next(state)
		})

		// Subscribe to barcode events
		barcodeEventStream.subscribe((barcode) => {
			if (state.ui.type === "shopping") {
				const products = state.ui.products
				products.set(barcode, 1 + (products.get(barcode) ?? 0))
				subscriber.next(state)
			}
		})

		apiPolling(state, subscriber, pollingInterval)
	})
}

// Tries to contact the server every 10 seconds.
// Pushes new values of the state to the given subscriber.
async function apiPolling(
	state: S.State,
	subscriber: R.Subscriber<S.State>,
	pollingInterval: number
) {
	// Start a loop of periodic polling (every 10s)
	while (true) {
		const [users, products] = await Promise.all([
			Api.getUsers(),
			Api.getProducts(),
			Api.attemptWalPush()
		])

		state.products = products
		state.users = users

		// The user's card might be ready to use
		if (state.ui.type === "connectingCard") {
			const user = S.getUserByCardHex(state.ui.hex, state.users)
			if (user !== undefined) state.ui = S.shopping(user.id, state.ui.hex)
		}

		subscriber.next(state)

		await sleep(pollingInterval)
	}
}
