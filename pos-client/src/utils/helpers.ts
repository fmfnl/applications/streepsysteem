/**
 * Promise-based wrapper around `setTimeout`.
 */
export function sleep(ms: number) {
	return new Promise((res) => {
		setTimeout(res, ms)
	})
}
