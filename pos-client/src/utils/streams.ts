import { useEffect, useState } from "preact/hooks"
import * as R from "rxjs"

/**
 * (P)react hook for subscribing to a rjxs observable.
 */
export function useObservable<T>(stream: R.Observable<T>, initial: T): T {
	// We want to bypass react's referene-equality change detection,
	// so we wrap the state in a new object each time.
	const [state, setState] = useState({ value: initial })

	useEffect(() => {
		const subscription = stream.subscribe((state) => {
			setState({ value: state })
		})

		return subscription.unsubscribe
	}, [stream])

	return state.value
}
