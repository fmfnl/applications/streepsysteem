export function getLocalStorage<T>(key: string, defaultValue: T): T {
	const cached = localStorage.getItem(key)
	return cached === null ? defaultValue : JSON.parse(cached)
}

export function setLocalStorage<T>(key: string, value: T) {
	localStorage.setItem(key, JSON.stringify(value))
}
