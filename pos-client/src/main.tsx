import { h, render } from "preact"
import { App } from "./app/ui"
import { state } from "./app/logic"
import "./index.css"

const root = document.getElementById("app")!

render(<App states={state()} />, root)
