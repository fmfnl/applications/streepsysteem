import type { CardHex } from "./api"
import * as R from "rxjs"

export type NfcEvent =
	| { type: "connected"; hex: CardHex }
	| { type: "released" }

export function nfcEvents(): R.Observable<NfcEvent> {
	return new R.Observable((subscriber) => {
		document.addEventListener("contextmenu", (e) => {
			e.preventDefault()

			subscriber.next({
				type: "released"
			})
		})

		document.addEventListener("click", (e) => {
			e.preventDefault()

			subscriber.next({
				type: "connected",
				hex: "asdfsdf"
			})
		})
	})
}
