import type { ProductBarcode } from "./api"
import * as R from "rxjs"

export function barcodeEvents(timeout = 10000): R.Observable<ProductBarcode> {
	return new R.Observable((subscriber) => {
		let accumulated = ""
		let lastTimestamp = Date.now()

		document.addEventListener("keypress", (e) => {
			const currentTimestamp = Date.now()
			if (currentTimestamp - lastTimestamp > timeout) accumulated = ""
			lastTimestamp = currentTimestamp

			if (e.key === "Enter") {
				subscriber.next(accumulated)
				accumulated = ""
			} else if (e.key.match(/^[a-zA-Z0-9]$/)) {
				accumulated += e.key
			}
		})
	})
}
