import createClient from "openapi-fetch"
import type { paths } from "../../generated/api.d.ts"
import { getLocalStorage, setLocalStorage } from "src/utils/localStorage.js"

const client = createClient<paths>({ baseUrl: process.env.API_URL })

export type Users =
	paths["/users"]["get"]["responses"]["200"]["content"]["application/json"]
export type User = Users[number]

export type Products =
	paths["/products"]["get"]["responses"]["200"]["content"]["application/json"]
export type Product = Products[number]

export type Transactions =
	paths["/transactions"]["post"]["requestBody"]["content"]["application/json"]
export type Transaction = Transactions[number]

export type CardHex = Users[number]["cards"][number]["hex"]
export type ProductId = Products[number]["id"]
export type ProductBarcode = Products[number]["barcode"]
export type UserId = Users[number]["id"]

/**
 * Takes in a basic fetching function and caches the result to local storage.
 */
async function cached<T>(
	key: string,
	defaultValue: T,
	f: () => Promise<{ data?: T; error?: unknown }>
): Promise<T> {
	try {
		const res = await f()
		if (!res.error && res.data) {
			setLocalStorage(key, res.data)
			return res.data
		} else {
			console.error(res.error)
		}
	} catch (err) {
		console.error(err)
	}

	return getLocalStorage<T>(key, defaultValue)
}

export const getUsers = () => cached("users", [], () => client.GET("/users"))
export const getProducts = () =>
	cached("products", [], () => client.GET("/products"))

/**
 * Sends an unknown card to the api. Returns `true` when the push was successful.
 */
export async function pushCard(hex: CardHex): Promise<boolean> {
	try {
		const res = await client.POST("/cards", { body: { hex } })
		if (res.error) return false
		return true
	} catch (err) {
		console.error(err)
		return false
	}
}

export function appendTransactionWal(transaction: Transaction) {
	const wal = getLocalStorage<Transactions>("transactions", [])
	wal.push(transaction)
	setLocalStorage("transactions", wal)
}

export async function attemptWalPush(): Promise<void> {
	const transactions = getLocalStorage<Transactions>("transactions", [])
	if (transactions.length === 0) return

	try {
		const res = await client.POST("/transactions", {
			body: transactions
		})

		if (!res.error) {
			// We must be super careful, as some new transactions might've been created
			// while the request was pending. That's why we re-read local storage here,
			// and perform the swap in a synchronous manner!
			const upToDateTransactions = getLocalStorage<Transactions>(
				"transactions",
				[]
			)

			setLocalStorage<Transactions>(
				"transactions",
				upToDateTransactions.slice(transactions.length)
			)
		}
	} catch (err) {
		console.error(err)
	}
}
