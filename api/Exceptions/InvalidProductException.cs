﻿namespace FMF.Streepsysteem.Backend.Exceptions
{
    public class InvalidProductException(ulong id) : IValidationErrorException($"Product with id '{id}' doesn't exists!");
}
