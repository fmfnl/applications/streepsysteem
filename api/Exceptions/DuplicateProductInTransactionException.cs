namespace FMF.Streepsysteem.Backend.Exceptions;

public class DuplicateProductInTransactionException(ulong productId, ulong transactionId)
    : IValidationErrorException($"Duplicate product with id '{productId}' in transaction with id '{transactionId}'");