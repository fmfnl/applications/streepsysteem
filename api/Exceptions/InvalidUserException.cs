using FMF.Streepsysteem.Backend.Exceptions;

namespace FMF.Streepsysteem.Backend.DbModels;

public class InvalidUserException(ulong id) : IValidationErrorException($"User with id '{id}' doesn't exists!");