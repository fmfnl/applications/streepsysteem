﻿namespace FMF.Streepsysteem.Backend.Exceptions
{
    public class DuplicateNfcEntryException(string hex)
        : IValidationErrorException($"The NFC card with hex '{hex}' already exists in the database!");
}
