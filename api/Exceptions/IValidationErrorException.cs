namespace FMF.Streepsysteem.Backend.Exceptions;

public abstract class IValidationErrorException(string message) : Exception(message);