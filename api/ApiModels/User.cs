namespace FMF.Streepsysteem.Backend.ApiModels;

public class Nfc
{
    public required ulong Id { get; set; }
    public required String Hex { get; set; }

    public static Nfc FromDbModel(DbModels.Nfc nfc) => new()
    {
        Id = nfc.Id,
        Hex = nfc.Hex,
    };
}

public class User
{
    public required ulong Id { get; set; }
    public required String Name { get; set; }
    public required List<Nfc> Cards { get; set; }

    public static User FromDbModel(DbModels.User user) => new()
    {
        Id = user.Id,
        Name = user.Name,
        Cards = user.Nfcs.Select(Nfc.FromDbModel).ToList(),
    };
}
