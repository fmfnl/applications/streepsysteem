using System.ComponentModel.DataAnnotations;
using FMF.Streepsysteem.Backend.CustomValidationAttributes;

namespace FMF.Streepsysteem.Backend.ApiModels;

public class NewNfc
{
    [Hex, Required]
    public required String Hex { get; set; }

    public DbModels.Nfc ToDbModel() => new()
    {
        Hex = Hex,
        Description = ""
    };
}
