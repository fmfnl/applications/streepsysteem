﻿using System.ComponentModel.DataAnnotations;
using FMF.Streepsysteem.Backend.Repositories;
using FMF.Streepsysteem.Backend.CustomValidationAttributes;

namespace FMF.Streepsysteem.Backend.ApiModels;

public class TransactionProduct
{
    [Required] 
    public required ulong Id { get; set; }
    [Required, Range(minimum: 1, maximum: ulong.MaxValue)] 
    public required uint Quantity { get; set; }

    public DbModels.TransactionProduct ToDbModel(
        DbModels.Transaction transaction, 
        DbModels.Product product
    ) => new()
    {
        UnitPrice = product.Price,
        Quantity = Quantity,
        Transaction = transaction,
        Product = product
    };
}

public class Transaction
{
    [Required] 
    public required ulong UserId { get; set; }
    [Required] 
    public required DateTime Time { get; set; }
    [Required, MinLength(1)] 
    public required List<TransactionProduct> Products { get; set; }

    public DbModels.Transaction ToDbModel(DbModels.User user) => new()
    {
        User = user,
        Date = Time
    };
}