namespace FMF.Streepsysteem.Backend.ApiModels;

public class Product
{
    public required ulong Id { get; set; }
    public required uint Price { get; set; }
    public required String Barcode { get; set; }
    public required String Name { get; set; }

    public static Product FromDbModel(DbModels.Product product) => new()
    {
        Id = product.Id,
        Price = product.Price,
        Barcode = product.Barcode,
        Name = product.Name,
    };
}
