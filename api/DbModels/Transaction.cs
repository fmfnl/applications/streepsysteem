﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FMF.Streepsysteem.Backend.DbModels;

[Table("transactions")]
public class Transaction
{
    [Key]
    [Column("transaction_id")]
    public ulong Id { get; set; }

    [Column("date")]
    public DateTime Date { get; set; } = DateTime.Now;

    [ForeignKey("user_id")]
    public required User User { get; set; }
    
    [ForeignKey("debit_id")]
    public Debit? Debit { get; set; } = null;

    public ICollection<TransactionProduct> TransactionProducts { get; set; } = [];
}
