using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace FMF.Streepsysteem.Backend.DbModels;

[Keyless]
[Table("transaction_products")]
public class TransactionProduct
{
    public required Transaction Transaction { get; set; }
    public required Product Product { get; set; }
    

    [Column("transaction_price")]
    public required uint UnitPrice { get; set; }

    [Column("transaction_amount")]
    public required uint Quantity { get; set; }
}
