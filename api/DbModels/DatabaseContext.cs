﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace FMF.Streepsysteem.Backend.DbModels;

public class DatabaseContext : DbContext
{
    public DbSet<User> Users { get; set; }
    public DbSet<Transaction> Transactions { get; set; }
    public DbSet<Product> Products { get; set; }
    public DbSet<Nfc> Nfcs { get; set; }
    public DbSet<Debit> Debits { get; set; }
    public DbSet<TransactionProduct> TransactionProducts { get; set; }

    public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) { }

    // Declare things like relations. Or whatever we could not put into the original model like conversion functions, default value in SQL, etc.
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Nfc>(entity =>
        {
            entity.Property(e => e.LastUsed)
                .ValueGeneratedOnAddOrUpdate();
        });

        modelBuilder.Entity<Product>()
            .Property(p => p.Btw)
            .HasConversion(
                v => v.ToString(),
                v => string.IsNullOrEmpty(v) ? 0 : uint.Parse(v));

        modelBuilder.Entity<Nfc>()
            .HasIndex(e => e.Hex)
            .IsUnique();

        modelBuilder.Entity<TransactionProduct>()
            .Property<ulong>("product_id");
        
        modelBuilder.Entity<TransactionProduct>()
            .Property<ulong>("transaction_id");
        
        modelBuilder.Entity<TransactionProduct>()
            .HasKey("transaction_id", "product_id");
        
        modelBuilder.Entity<TransactionProduct>()
            .HasOne<Transaction>(s => s.Transaction)
            .WithMany(s => s.TransactionProducts)
            .HasForeignKey("transaction_id")
            .IsRequired();

        modelBuilder.Entity<TransactionProduct>()
            .HasOne<Product>(s => s.Product)
            .WithMany()
            .HasForeignKey("product_id")
            .IsRequired();

        modelBuilder.Entity<Transaction>()
            .HasOne<Debit>(s => s.Debit)
            .WithMany()
            .HasForeignKey("debit_id");
    }
}
