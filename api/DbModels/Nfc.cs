using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace FMF.Streepsysteem.Backend.DbModels;

[Table("nfc")]
[Index(nameof(Hex), IsUnique = true)]
public class Nfc
{
    [Key]
    [Column("relation_id")]
    public ulong Id { get; set; }

    [Column("user_id")] 
    public ulong UserId { get; set; }

    [MaxLength(255)]
    public required string Hex { get; set; }

    [MaxLength(255)]
    public string? Description { get; set; }

    [Column("last_used")]
    public DateTime LastUsed { get; set; }
}
