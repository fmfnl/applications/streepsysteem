﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FMF.Streepsysteem.Backend.DbModels;

[Table("users")]
public class User
{
    [Key]
    [Column("user_id")]
    public ulong Id { get; set; }

    [MaxLength(255)]
    [Column("fmf_id")]
    public required string FmfId { get; set; }

    [MaxLength(255)]
    public required string Name { get; set; }

    [MaxLength(255)]
    public required string Email { get; set; }

    public ICollection<Transaction> Transactions { get; } = [];
    public ICollection<Nfc> Nfcs { get; } = [];
}
