using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FMF.Streepsysteem.Backend.DbModels;

[Table("debits")]
public class Debit
{
    [Key]
    [Column("debit_id")]
    public ulong Id { get; set; }
    public required DateTime Timestamp { get; set; }

    [MaxLength(255)]
    public string? Description { get; set; }
}
