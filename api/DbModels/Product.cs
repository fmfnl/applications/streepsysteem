﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace FMF.Streepsysteem.Backend.DbModels;

[Table("products")]
[Index(nameof(Barcode), IsUnique = true)]
public class Product
{
    [Key]
    [Column("product_id")]
    public ulong Id { get; set; }

    public required uint Price { get; set; }

    [MaxLength(255)]
    [Column("product_name")]
    public required string Name { get; set; }

    [MaxLength(255)]
    public required string Barcode { get; set; }

    public required uint Btw { get; set; }
}
