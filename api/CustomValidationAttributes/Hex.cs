using System;
using System.Text.RegularExpressions;
using System.Globalization;
using System.ComponentModel.DataAnnotations;

namespace FMF.Streepsysteem.Backend.CustomValidationAttributes;

[AttributeUsage(AttributeTargets.Property | 
                AttributeTargets.Field, 
                AllowMultiple = false)]
public sealed partial class Hex : ValidationAttribute
{
    public override bool IsValid(object? value)
    {
        if (value is not string valueString) return false;
        
        if (valueString.Contains('\n')) return false;

        if (!MyRegex().IsMatch((string)value)) return false;
        
        return true;
    }

    public override string FormatErrorMessage(string name) => "Invalid hex code";

    [GeneratedRegex("^(?:[0-9A-F]{2} ){0,31}[0-9A-F]{2}$")]
    private static partial Regex MyRegex();
}