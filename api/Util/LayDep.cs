﻿namespace FMF.Streepsysteem.Backend.Util;

public class LazyDep<T> : Lazy<T> where T : class
{
    public LazyDep(IServiceProvider provider) : base(provider.GetService<T>) { }
}
