using DotNetEnv;
using DotNetEnv.Configuration;
using Microsoft.EntityFrameworkCore;
using FMF.Streepsysteem.Backend.DbModels;
using FMF.Streepsysteem.Backend.Repositories;
using FMF.Streepsysteem.Backend.Util;

/*
 * Entry point of the application. Pretend like we are inside main() right now.
 */

var builder = WebApplication.CreateBuilder(args);

builder.Configuration.AddDotNetEnvMulti([".env", "dotnet.env"], LoadOptions.TraversePath().NoClobber());
builder.Configuration.AddEnvironmentVariables();

// Add database connection.
var dbConnectionString = builder.Configuration.GetConnectionString("defaultConnection");
builder.Services.AddDbContext<DatabaseContext>(options =>
    options.UseMySql(dbConnectionString, ServerVersion.AutoDetect(dbConnectionString)));

// Add services to the container.
// Scoped: Lifetime of a single request.
builder.Services.AddScoped<ProductRepository>();
builder.Services.AddScoped<TransactionRepository>();
builder.Services.AddScoped<UserRepository>();
builder.Services.AddScoped<NfcRepository>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

foreach (var service in builder.Services.ToList())
{
    var lazyType = typeof(Lazy<>);
    lazyType = lazyType.MakeGenericType(service.ServiceType);

    var lazyDepType = typeof(LazyDep<>);
    lazyDepType = lazyDepType.MakeGenericType(service.ServiceType);

    builder.Services.Add(new ServiceDescriptor(lazyType, lazyDepType, service.Lifetime));
}

// Fallback
builder.Services.AddTransient(typeof(Lazy<>), typeof(LazyDep<>));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();
