﻿using FMF.Streepsysteem.Backend.DbModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FMF.Streepsysteem.Backend.Repositories;
using Microsoft.Extensions.Options;
using TransactionProduct = FMF.Streepsysteem.Backend.ApiModels.TransactionProduct;
using FMF.Streepsysteem.Backend.Exceptions;

namespace FMF.Streepsysteem.Backend.Controllers;

[ApiController]
[Route("/")]
[Produces("application/json")]
public class MainController : ControllerBase
{
    /*
     * For now, everything is put into one controller because there are only a few endpoints.
     * Let's however split it into multiple controllers as soon as we need to make more endpoints.
     */

    #region GET

    [HttpGet("users")]
    public async Task<ActionResult<List<ApiModels.User>>> GetUsers([FromServices] UserRepository _users)
    {
        var users = await _users.Collection
            .Include(u => u.Nfcs)
            .AsAsyncEnumerable()
            .Select(ApiModels.User.FromDbModel)
            .ToListAsync();
        return Ok(users);
    }

    [HttpGet("products")]
    public async Task<ActionResult<List<ApiModels.Product>>> GetProducts([FromServices] ProductRepository _products)
    {
        return Ok(await 
            _products.Collection.AsAsyncEnumerable().Select(ApiModels.Product.FromDbModel).ToListAsync());
    }
    
    #endregion


    #region POST

    [HttpPost("cards")]
    [ProducesResponseType(typeof(string), 200)]
    [ProducesResponseType(400)]
    public async Task<IActionResult> CreateCard(
        [FromQuery] ApiModels.NewNfc newNfc,
        [FromServices] IOptions<ApiBehaviorOptions> apiBehaviorOptions,
        [FromServices] NfcRepository _nfcs,
        [FromServices] DatabaseContext _databaseContext
    )
    {
        try
        {
            await _nfcs.CreateAsync(newNfc.ToDbModel());
            await _databaseContext.SaveChangesAsync();
        }
        catch (IValidationErrorException e)
        {
            ModelState.AddModelError(nameof(newNfc), e.ToString());
            return apiBehaviorOptions.Value.InvalidModelStateResponseFactory(ControllerContext);
        }
        catch (Exception e)
        {
            ModelState.AddModelError(nameof(newNfc), "Failed to write nfc card to the database");
            Console.Write(e.ToString());
            return apiBehaviorOptions.Value.InvalidModelStateResponseFactory(ControllerContext);
        }

        return Ok("Successfully registered card");
    }

    [HttpPost("transactions")]
    [ProducesResponseType(typeof(string), 200)]
    [ProducesResponseType(400)]
    public async Task<IActionResult> PushTransactions(
        List<ApiModels.Transaction> transactions,
        [FromServices] IOptions<ApiBehaviorOptions> apiBehaviorOptions,
        [FromServices] DatabaseContext _databaseContext,
        [FromServices] UserRepository _users,
        [FromServices] TransactionRepository _transactions,
        [FromServices] ProductRepository _products
    )
    {
        try
        {
            foreach (ApiModels.Transaction transaction in transactions)
            {
                DbModels.User dbUser = await _users.GetAsync(transaction.UserId);

                DbModels.Transaction dbTransaction = transaction.ToDbModel(dbUser);
                await _transactions.CreateAsync(dbTransaction);

                foreach (TransactionProduct transactionProduct in transaction.Products)
                {
                    DbModels.Product dbProduct = await _products.GetAsync(transactionProduct.Id);
                    DbModels.TransactionProduct dbTransactionProduct = 
                        transactionProduct.ToDbModel(dbTransaction, dbProduct);
                    await _transactions.CreateTransactionProductAsync(dbTransactionProduct);
                }
            }
            await _databaseContext.SaveChangesAsync();
        }
        catch (IValidationErrorException e)
        {
            ModelState.AddModelError(nameof(transactions), e.Message);
            return apiBehaviorOptions.Value.InvalidModelStateResponseFactory(ControllerContext);
        }
        catch (Exception e)
        {
            ModelState.AddModelError(nameof(transactions), "Failed to write transactions to the database");
            Console.Write(e.ToString());
            return apiBehaviorOptions.Value.InvalidModelStateResponseFactory(ControllerContext);
        }

        return Ok("Successfully pushed transactions");
    }

    #endregion
}