﻿using System.Diagnostics.CodeAnalysis;
using FMF.Streepsysteem.Backend.DbModels;
using FMF.Streepsysteem.Backend.Exceptions;

namespace FMF.Streepsysteem.Backend.Repositories;

public class ProductRepository : IRepository<Product, ulong>
{
    private readonly DatabaseContext _context;

    public ProductRepository(DatabaseContext context)
    {
        _context = context;
    }

    public IQueryable<Product> Collection => _context.Products;

    [return: NotNull]
    public async Task<Product> GetAsync(ulong key)
    {
        Product? product = await _context.Products.FindAsync(key);
        if (product == null)
        {
            throw new InvalidProductException(key);
        }

        return product;
    }

    public async Task CreateAsync(Product entity)
    {
        _context.Products.Add(entity);
    }

    public async Task UpdateAsync(Product entity)
    {
        _context.Products.Update(entity);
    }

    public async Task DeleteAsync(Product entity)
    {
        _context.Products.Remove(entity);
    }
}