﻿using FMF.Streepsysteem.Backend.DbModels;
using FMF.Streepsysteem.Backend.Exceptions;
using TransactionProduct = FMF.Streepsysteem.Backend.DbModels.TransactionProduct;

namespace FMF.Streepsysteem.Backend.Repositories;

public class TransactionRepository : IRepository<Transaction, ulong>
{
    private readonly DatabaseContext _context;

    public TransactionRepository(DatabaseContext context)
    {
        _context = context;
    }

    public IQueryable<Transaction> Collection => _context.Transactions;

    public async Task<Transaction?> GetAsync(ulong key)
    {
        return await _context.Transactions.FindAsync(key);
    }

    public async Task CreateAsync(Transaction entity)
    {
        _context.Transactions.Add(entity);
    }

    public async Task CreateTransactionProductAsync(TransactionProduct entity)
    {
        try
        {
            _context.TransactionProducts.Add(entity);
        }
        catch (InvalidOperationException e)
        {
            if (e.ToString().Contains("another instance with the same key"))
                throw new DuplicateProductInTransactionException(entity.Product.Id, entity.Transaction.Id);
            throw;
        }
        
    }

    public async Task UpdateAsync(Transaction entity)
    {
        _context.Transactions.Update(entity);
    }

    public async Task DeleteAsync(Transaction entity)
    {
        _context.Transactions.Remove(entity);
    }
}