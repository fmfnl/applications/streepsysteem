﻿namespace FMF.Streepsysteem.Backend.Repositories;

public interface IRepository<T, TKey> where T : class
{
    IQueryable<T> Collection { get; }
    Task<T?> GetAsync(TKey key);
    Task CreateAsync(T entity);
    Task UpdateAsync(T entity);
    Task DeleteAsync(T entity);
}
