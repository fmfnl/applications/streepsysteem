﻿using System.Diagnostics.CodeAnalysis;
using FMF.Streepsysteem.Backend.DbModels;

namespace FMF.Streepsysteem.Backend.Repositories;

public class UserRepository : IRepository<User, ulong>
{
    private readonly DatabaseContext _context;

    public UserRepository(DatabaseContext context)
    {
        _context = context;
    }

    public IQueryable<User> Collection => _context.Users;

    [return: NotNull]
    public async Task<User> GetAsync(ulong key)
    {
        User? user = await _context.Users.FindAsync(key);
        if (user == null)
        {
            throw new InvalidUserException(key);
        }

        return user;
    }

    public async Task CreateAsync(User entity)
    {
        _context.Users.Add(entity);
    }

    public async Task UpdateAsync(User entity)
    {
        _context.Users.Update(entity);
    }

    public async Task DeleteAsync(User entity)
    {
        _context.Users.Remove(entity);
    }
}