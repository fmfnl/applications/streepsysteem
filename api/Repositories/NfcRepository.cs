using FMF.Streepsysteem.Backend.DbModels;
using Microsoft.EntityFrameworkCore;
using FMF.Streepsysteem.Backend.Exceptions;
using System;

namespace FMF.Streepsysteem.Backend.Repositories;

public class NfcRepository : IRepository<Nfc, ulong>
{
    private readonly DatabaseContext _context;

    public NfcRepository(DatabaseContext context)
    {
        _context = context;
    }

    public IQueryable<Nfc> Collection => _context.Nfcs;

    public async Task<Nfc?> GetAsync(ulong key)
    {
        return await _context.Nfcs.FindAsync(key);
    }

    public async Task CreateAsync(Nfc entity)
    {
        try
        {
            _context.Nfcs.Add(entity);
        }
        catch (DbUpdateException e)
        {
            if (e.ToString().Contains("Duplicate entry"))
            {
                throw new DuplicateNfcEntryException(entity.Hex);
            }

            throw;
        }
    }

    public async Task UpdateAsync(Nfc entity)
    {
        _context.Nfcs.Update(entity);
    }

    public async Task DeleteAsync(Nfc entity)
    {
        _context.Nfcs.Remove(entity);
    }
}